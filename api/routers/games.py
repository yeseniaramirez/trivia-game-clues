from sqlite3 import Timestamp
from time import time
from fastapi import APIRouter, Response, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import psycopg

from .clues import Clue

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()

class Game(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_amount_won: int

class CustomGame(BaseModel):
    id: int
    created_on: str
    clues: list[Clue]

class Message(BaseModel):
    message: str

@router.get(
    "/api/games/{game_id}",
    response_model=Game,
    responses={404: {"model": Message}},
)
def get_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT g.id, g.episode_id, g.aired, g.canon, SUM(clues.value) AS total_amount_won
                FROM games AS g
                LEFT OUTER JOIN clues
                    ON (clues.game_id = g.id)
                WHERE game_id = %s
                GROUP BY g.id
            """,
                [game_id]
            )

            row = cur.fetchone()
            if row is None:
                return JSONResponse(status_code=404, content={"message": "Game not found"})
                # response.status_code = status.HTTP_404_NOT_FOUND
                # return {"message": "Game not found"}
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record

@router.get(
    "/api/custom-games",
    response_model=Game,
    responses={404: {"model": Message}},
)
def create_game(game: Game, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute("""
            SELECT
                clues.id, 
                clues.answer, 
                clues.question, 
                clues.value, 
                clues.invalid_count,
                categories.id, 
                categories.title, 
                categories.canon
            FROM clues
            INNER JOIN categories 
                ON (clues.category_id = categories.id)
            WHERE clues.canon = true 
            ORDER BY RANDOM() LIMIT 30
            """)

            results = []
            game = cur.fetchall()
            for row in game:
                record = {}
                for i, column in enumerate(cur.description):
                    record[column.name] = row[i]
                results.append(record)
            
            cur.execute(
                """
                SELECT COUNT(*) FROM clues
            """
            )

            return Game(games=results)
