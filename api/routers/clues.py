from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from typing import Union

from .categories import CategoryOut

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Message(BaseModel):
    message: str


@router.get(
    "/api/random-clue/{clue_id}", 
    response_model=Clue, 
    responses={404: {"model": Message}}
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur: 
            cur.execute("""
            SELECT 
                clues.id, 
                clues.answer, 
                clues.question, 
                clues.value, 
                clues.invalid_count, 
                clues.canon, 
                categories.id, 
                categories.title, 
                categories.canon
            FROM clues
            INNER JOIN categories ON (clues.category_id = categories.id)
            WHERE clues.id = %s
            """, [clue_id])

            clue = cur.fetchone()
            if clue is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            return {
                    "id": clue[0],
                    "answer": clue[1],
                    "question": clue[2],
                    "value": clue[3],
                    "invalid_count": clue[4],
                    "category": {
                        "id": clue[6],
                        "title": clue[7],
                        "canon": clue[8]
                    },
                    "canon": clue[5]
            }


@router.get(
    "/api/random-clue",
    response_model=Union [Clue, Message],
    responses={404: {"model": Message}},
)
def random_clue(valid:bool=True):
    with psycopg.connect() as conn:
        with conn.cursor() as cur: 
            if valid == True:
                cur.execute(f"""
                SELECT 
                    clues.id, 
                    clues.answer, 
                    clues.question, 
                    clues.value, 
                    clues.invalid_count, 
                    clues.canon, 
                    categories.id, 
                    categories.title, 
                    categories.canon
                FROM clues
                INNER JOIN categories ON (clues.category_id = categories.id)
                WHERE clues.invalid_count = 0
                ORDER BY random() 
                """)
            else:
                cur.execute(f"""
                SELECT 
                    clues.id, 
                    clues.answer, 
                    clues.question, 
                    clues.value, 
                    clues.invalid_count, 
                    clues.canon, 
                    categories.id, 
                    categories.title, 
                    categories.canon
                FROM clues
                INNER JOIN categories ON (clues.category_id = categories.id)
                ORDER BY random() 
                """)

            clue = cur.fetchone()

            return({
                    "id": clue[0],
                    "answer": clue[1],
                    "question": clue[2],
                    "value": clue[3],
                    "invalid_count": clue[4],
                    "category": {
                        "id": clue[6],
                        "title": clue[7],
                        "canon": clue[8],
                    },
                    "canon": clue[5]
            })


@router.get(
    "/api/clues",
    response_model=Union [Clue, Message],
    responses={404: {"model": Message}},
)
def random_clue(valid: bool=True, page: int=0):
    with psycopg.connect() as conn:
        with conn.cursor() as cur: 
            cur.execute(f"""
            SELECT 
                clues.id, 
                clues.answer, 
                clues.question, 
                clues.value, 
                clues.invalid_count, 
                clues.canon, 
                categories.id, 
                categories.title, 
                categories.canon
            FROM clues
            INNER JOIN categories ON (clues.category_id = categories.id)
            WHERE clues.canon = %s AND clues.invalid_count = 0
            ORDER BY random() 
            """, [valid])

            clue = cur.fetchone()

            return ({
                    "id": clue[0],
                    "answer": clue[1],
                    "question": clue[2],
                    "value": clue[3],
                    "invalid_count": clue[4],
                    "category": {
                        "id": clue[6],
                        "title": clue[7],
                        "canon": clue[8],
                    },
                    "canon": clue[5]
            })
